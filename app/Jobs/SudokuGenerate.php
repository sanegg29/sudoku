<?php

namespace App\Jobs;

use App\Models\Sudoku;
use App\Services\SudokuService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SudokuGenerate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 10;
    public $timeout = 1800;
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // time for generate new fields = 1-10 min
            $c = new SudokuService();
            $fields = $c->newFields();
            foreach ($fields as $field) {
//                AddSudoku::dispatch($field)
//                    ->onConnection("database")
//                    ->delay(now()->addSeconds(1));
                $this->sudokuSave($field);
            }
    }

    private function sudokuSave (array $field) {
        $difficulty = SudokuService::sudokuSolutionDifficulty($field[0],$field[1]);
        if (Sudoku::where('difficulty',$difficulty)->count()<10000) {
//            dump('sudokuSave is started mass with difficulty='.$difficulty);
            Log::info('sudokuSave is started mass with difficulty='.$difficulty);
//            $mode = 0;
            $mode = rand(1,2);
            $fields = SudokuService::mix($field[0],$mode);
            $answers = SudokuService::mix($field[1],$mode);
            foreach ($fields as $key=>$strfield) {
                Sudoku::firstOrCreate([
                        "field" => $fields[$key],
                        "solution" => $answers[$key]]
//                        "difficulty"=>$difficulty]
                );
            }

        } else {
            Sudoku::firstOrCreate([
                    "field" => $field[0],
                    "solution" => $field[1]]
            );
        }
    }
}
