<?php

namespace App\Observers;

use App\Models\Sudoku;
use App\Services\SudokuService;

class SudokuObserver
{
    /**
     * Handle the Sudoku "created" event.
     *
     * @param  \App\Models\Sudoku  $sudoku
     * @return void
     */
    public function created(Sudoku $sudoku)
    {
        //
    }
    public function creating(Sudoku $sudoku)
    {
        if (!$sudoku->solution){
//            $x = new SudokuService();
//            SudokuService::sudokuSolutions()
            $solutions = SudokuService::sudokuSolutions($sudoku->field);
            if (count($solutions) === 1) {
                $sudoku->solution = current(SudokuService::sudokuSolutions($sudoku->field));
            }else {
                $sudoku->delete();
            }
        }
        if (!$sudoku->difficulty){
            $sudoku->difficulty = SudokuService::sudokuSolutionDifficulty($sudoku->field, $sudoku->solution);
        }
    }

    /**
     * Handle the Sudoku "updated" event.
     *
     * @param  \App\Models\Sudoku  $sudoku
     * @return void
     */
    public function updated(Sudoku $sudoku)
    {
        dump('hi');
//        dump($sudoku->users()->count());
//        $sudoku->users()->touch();

    }
    public function updating(Sudoku $sudoku)
    {
//        $sudoku->difficulty = SudokuService::sudokuSolutionDifficulty($sudoku->field, $sudoku->solution);
    }

    /**
     * Handle the Sudoku "deleted" event.
     *
     * @param  \App\Models\Sudoku  $sudoku
     * @return void
     */
    public function deleted(Sudoku $sudoku)
    {
        //
    }

    /**
     * Handle the Sudoku "restored" event.
     *
     * @param  \App\Models\Sudoku  $sudoku
     * @return void
     */
    public function restored(Sudoku $sudoku)
    {
        //
    }

    /**
     * Handle the Sudoku "force deleted" event.
     *
     * @param  \App\Models\Sudoku  $sudoku
     * @return void
     */
    public function forceDeleted(Sudoku $sudoku)
    {
        //
    }
}
