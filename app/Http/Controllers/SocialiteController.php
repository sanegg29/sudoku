<?php

namespace App\Http\Controllers;

use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class SocialiteController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $socialiteUser = Socialite::driver($provider)->stateless()->user();

        $user = $this->findOrCreateUser($provider, $socialiteUser);

        $token = $user->createToken(Str::random(8))
            ->plainTextToken;

        return view('callback', [
            'token' => $token,
        ]);
    }

    private function findOrCreateUser($provider, $socialiteUser)
    {
        if ($user = $this->findUserBySocialId($provider, $socialiteUser->getId())) {
            return $user;
        }

        if ($user = $this->findUserByEmail($provider, $socialiteUser->getEmail())) {
            $this->addSocialAccount($provider, $user, $socialiteUser);

            return $user;
        }

        $name = $socialiteUser->getName() ? $socialiteUser->getName() : $socialiteUser->nickname;

        $user = User::create([
            'name' => $name,
            'email' => $socialiteUser->getEmail(),
            'password' => bcrypt(Str::random(25)),
        ]);

        $this->addSocialAccount($provider, $user, $socialiteUser);

        return $user;
    }

    private function findUserBySocialId($provider, $id)
    {
        $socialAccount = SocialAccount::where('provider', $provider)
            ->where('provider_id', $id)
            ->first();

        return $socialAccount ? $socialAccount->user : false;
    }

    private function findUserByEmail($provider, $email)
    {
        return !$email ? null : User::where('email', $email)->first();
    }

    private function addSocialAccount($provider, $user, $socialiteUser)
    {
        SocialAccount::create([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $socialiteUser->getId(),
            'token' => $socialiteUser->token,
        ]);
    }

    public function vkMiniAppLogIn(Request $request)
    {


        $url = $request->data;

//        $url = $url->url->message;

        $url = 'https://example.com/?' . $url;
//        dump($url);

        $client_secret = env("VKONTAKTE_MINI_APP_SECRET");  //Защищённый ключ из настроек вашего приложения

        $query_params = [];
        parse_str(parse_url($url, PHP_URL_QUERY), $query_params); // Получаем query-параметры из URL

        $sign_params = [];
        foreach ($query_params as $name => $value) {
            if (strpos($name, 'vk_') !== 0) { // Получаем только vk параметры из query
                continue;
            }

            $sign_params[$name] = $value;
        }

        ksort($sign_params); // Сортируем массив по ключам
        $sign_params_query = http_build_query($sign_params); // Формируем строку вида "param_name1=value&param_name2=value"
        $sign = rtrim(strtr(base64_encode(hash_hmac('sha256', $sign_params_query, $client_secret, true)), '+/', '-_'), '='); // Получаем хеш-код от строки, используя защищеный ключ приложения. Генерация на основе метода HMAC.

        $status = $sign === $query_params['sign']; // Сравниваем полученную подпись со значением параметра 'sign'


        if ($status) {
            $user = $this->findOrCreateUserVk('vkontakte', $sign_params['vk_user_id']);
        } else {
            return response()->json([
                "message" => 'fail',
            ]);
        }

        $token = $user->createToken(Str::random(8))
            ->plainTextToken;

        return response()->json([
            "message" => "you are logged in",
            "token" => $token,
        ]);
    }

    private function findOrCreateUserVk($provider, $id)
    {
        if ($user = $this->findUserBySocialId($provider, $id)) {
            return $user;
        }

//        $key = env('VKONTAKTE_MINI_APP_SERVICE');
//        $addres = 'https://api.vk.com/method/users.get?user_id=' . $id. '&access_token=' . $key . '&v=5.52';
//        $json = json_decode(file_get_contents($addres), true);
//        $name = reset($json['response'])['first_name'];


        $user = User::create([
            'name' => 'vk_user' . rand(1, 10000),
            'password' => bcrypt(Str::random(25)),
        ]);

        SocialAccount::create([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $id,
            'token' => bcrypt(Str::random(25)),
        ]);
        return $user;
    }
}
