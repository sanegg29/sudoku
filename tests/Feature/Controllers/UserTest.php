<?php

namespace Tests\Feature\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Sudoku;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_success()
    {
        User::factory()->count(20)->create();

        $response = $this->getJson(
            route("user.index")
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_index_search_success()
    {

        User::factory()->create([
            "name" => "Ivan Vanko",
            "email" => "example@example1.com"
        ]);

        $response = $this->getJson(
            route("user.index", [
                "email" => "example@"
            ])
        );

        $response
            ->dump()
            ->assertStatus(200);

        $response = $this->getJson(
            route("user.index", [
                "name" => "Ivan"
            ])
        );

        $response
            ->dump()
            ->assertStatus(200);

        $response = $this->getJson(
            route("user.index", [
                "name" => "Ivan",
                "email" => "example"
            ])
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_show_success()
    {
        $user = User::factory()->create();

        $response = $this->getJson(
            route("user.show", [
                "user" => $user->id
            ])
        );
        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_show_failure()
    {
        $response = $this->getJson(
            route("user.show", [
                "user" => 111
            ])
        );
        $response
            ->dump()
            ->assertStatus(404);
    }

    public function test_update_success()
    {
        $user = User::factory()->create();

        $response = $this->putJson(
            route("user.update", [
                "user" => $user->id
            ]), [
                "name" => "test",
                "email" => "example@example.com"
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_update_failure()
    {
        $response = $this->putJson(
            route("user.update", [
                "user" => 13213
            ]), [
                "name" => "test",
                "email" => "example@example.com"
            ]
        );

        $response
            ->dump()
            ->assertStatus(404);
    }

    public function test_destroy_success()
    {
        $user = User::factory()->create();

        $response = $this->deleteJson(
            route("user.destroy", [
                "user" => $user->id
            ]));
        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_destroy_failure()
    {
        $response = $this->deleteJson(
            route("user.destroy", [
                "user" => 1134
            ]));
        $response
            ->dump()
            ->assertStatus(404);
    }

    public function test_addSolved_sudoku_success() {
//        $user = User::factory()->create();
//
//        Sanctum::actingAs($user);
//
//
//        $response = $this->postJson(
//            route("sudoku.solve", [
//                "id" => 1
//            ]));
//        $response->dump();
    }

    public function test_scores_success() {
        $response = $this->getJson(
            route("users.ranking")
        );


        $response
            ->dump()
            ->assertStatus(200);
    }

}
