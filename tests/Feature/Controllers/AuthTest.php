<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function test_signup_success()
    {
        $response = $this->postJson(
            route("auth.signup"), [
                "name" => "Test Test Test",
                "email" => "authtest@mail.local",
                "password" => "1234567890"
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_signin_success()
    {
        $user = User::factory()->create([
            "password" => "123456789"
        ]);

        $response = $this->postJson(
            route("auth.signin"), [
                "email" => $user->email,
                "password" => "123456789"
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_signin_wrong_password_failure()
    {
        $user = User::factory()->create([
            "password" => "1234567890"
        ]);

        $response = $this->postJson(
            route("auth.signin"), [
                "email" => $user->email,
                "password" => "123456789"
            ]
        );

        $response
            ->dump()
            ->assertStatus(401);
    }

    public function test_check_success()
    {
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $response = $this->getJson(
            route("auth.check"));

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_logout_success()
    {
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $response = $this->getJson(
            route("auth.logout"));

        $response
            ->dump()
            ->assertStatus(200);
    }

    public function test_vksignin_success()
    {
//        $user = User::factory()->create([
//            "password" => "123456789"
//        ]);

        $response = $this->getJson(
            route("auth.vksignin"), [
//                "email" => $user->email,
                "password" => "123456789"
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }
}
