<?php

namespace Tests\Feature\Controllers;

use App\Models\Sudoku;
use App\Models\User;
use App\Services\SudokuService;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SudokuTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_index_success()
    {
        $str = ["000000001000000023004056000000007400020000000030000080000230006000800000107000000",
//            '000000001000000023004005000000004600000070000120000000000010000000380700086000500',
//            '000000001000002000003000045000000200005010000020006700000450003600000800700000000',
//            '000000001000000023004005000000010000000030600078000500000060700000408000120000000',
//            "000000000000001002003000040000000500000060030020704000000028100004007000306000000",
//            "000000000000001002001034000000000010020050006300700000000008000005000030069200000",
//            "000000001000000023004056000000005000070000030230000080000080600000200000401000000",
//            "000000000000001002003000040000000005006370000010002006000400030180000000200000070",
//            "000000000000001002003000045000040000000230000061000700000800003070090000600000100",
//            '000000000000000012003004000000000000000053600170000080000100500006000403700800000',
//            '000000000000000001023045000000000230006000040700800000000030050100000000800090007'
        ];

        $response = $this->getJson(
            route("sudoku.index")
        );

//        $response
//            ->dump()
//            ->assertStatus(200);

    }

    public function test_index_success_with_user()
    {
        $str = "000000000000000012003004005000000630020007000060000000000260800005100000009000700";
        $str = SudokuService::mix($str);
        foreach ($str as $field) {
            Sudoku::firstOrCreate([
                "field" => $field
            ]);
        }
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $user->solvedSudoku()->attach(Sudoku::take(30)->pluck("id"));

//        dump($user->solvedSudoku()->pluck("id"));

        $response = $this->getJson(
            route("sudoku.index")
        );
        $response
            ->dump()
            ->assertStatus(200);


//        dump($str);
    }

    public function test_add_solve_success()
    {
        $str = "000000000000000012003004005000000630020007000060000000000260800005100000009000700";
        $str = SudokuService::mix($str);
        foreach ($str as $field) {
            Sudoku::updateOrCreate([
                "field" => $field
            ]);
        }

//        $user = User::factory()->create();

        $user = User::first();

        Sanctum::actingAs($user);

        dump($user->score);

//        dump(Sudoku::find(1)->get()->first()->solution);

        $response = $this->postJson(
            route("sudoku.solve", ["fields" => [
                [
                    "id" => 1,
                    "solution" => Sudoku::find(1)->solution
                ],
                ["id" => 2,
                    "solution" => Sudoku::find(2)->solution
                ],
                ["id" => 3,
                    "solution" => Sudoku::find(3)->solution
                ],
                [
                    "id" => 4,
                    "solution" => Sudoku::find(4)->solution
                ],
                ["id" => 2,
                    "solution" => Sudoku::find(2)->solution
                ],
                ["id" => 3,
                    "solution" => Sudoku::find(3)->solution
                ],
            ]])
        );

//        self::assertTrue(count($response->getOriginalContent()["message"]) === 3);

        $response
            ->dump()
            ->assertStatus(200);

        dump($user->score);
    }
    public function test_add_solve_partical_success()
    {
        $str = "000000000000000012003004005000000630020007000060000000000260800005100000009000700";
        $str = SudokuService::mix($str);
        foreach ($str as $field) {
            Sudoku::updateOrCreate([
                "field" => $field
            ]);
        }

        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $response = $this->postJson(
            route("sudoku.solve", ["fields" => [
                [
                    "id" => 1,
                    "solution" => '942516378856793412173824965437265891685179243219348756591482637324657189768931524'
                ],
                ["id" => 2,
                    "solution" => '378942516412856793965173824891437265243685179756219348637591482189324657524768931'
                ],
                ["id" => 3,
                    "solution" => '942516378856793412173824965437265891685179243219348756591482637324657189768931524'
                ],
            ]])
        );

        self::assertTrue(count($response->getOriginalContent()["message"]) === 2);

        $response
            ->dump()
            ->assertStatus(200);
    }
}
