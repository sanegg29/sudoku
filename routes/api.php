<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Socialite\GitHubController;
use App\Http\Controllers\Socialite\VkController;
use App\Http\Controllers\SocialiteController;
use App\Http\Controllers\SudokuController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource("/user", UserController::class);

Route::group([
    "prefix" => "users",
    "as"=> "users."
], function () {
    Route::get("/ranking", [UserController::class, "ranking"])->name("ranking");
});

Route::group([
    "prefix" => "auth",
    "as"=> "auth."
], function () {
    Route::post("signup", [AuthController::class, "signup"])->name("signup");
    Route::post("signin", [AuthController::class, "signin"])->name("signin");
    Route::get("check", [AuthController::class, "check"])
        ->middleware(["auth:sanctum"])
        ->name("check");
    Route::get("logout", [AuthController::class, "logout"])
        ->middleware(["auth:sanctum"])
        ->name("logout");
});

Route::group([
    "prefix" => "sudoku",
    "as"=> "sudoku."
], function () {
    Route::get("/generate", [SudokuController::class, "sudokuGenerate"])->name("generate");
    Route::get("/index", [SudokuController::class, "index"])->name("index");
    Route::post("/solve", [SudokuController::class, "addSolvedSudoku"])->name("solve");
});


Route::prefix('login')
    ->name('login.')
    ->middleware(['web'])
    ->group(function () {
        Route::get('/{provider}',[SocialiteController::class, 'redirect'])->name("redirect");
        Route::get('/{provider}/callback',[SocialiteController::class, 'callback'])->name("callback");
});
Route::post('/vkMiniApp/',[SocialiteController::class, 'vkMiniAppLogIn'])->name("vkMiniAppLogIn");



// подтверждение почты
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');
