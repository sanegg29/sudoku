<html>
<head>
    <meta charset="utf-8">
    <title>Callback</title>
    <script>
        window.opener.postMessage({ token: "{{ $token }}"}, '*');
        // window.opener.postMessage('hi', '*');
        window.close();
    </script>
</head>
<body>
</body>
</html>
